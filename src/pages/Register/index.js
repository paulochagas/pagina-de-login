import React from 'react';
import style from './style.module.scss';
import Form from '../../components/Form';
import Input from '../../components/Input';
import arrayOfInputs from './arrayOfInputs';
import H1 from '../../components/H1';

const Register = ({className}) => {
    const inputs = arrayOfInputs.map(({type, placeholder, value, onClick, id})=> (
        <Input key={placeholder || value} type={type} className="register" placeholder={placeholder} value={value} onClick={onClick} />
    ))

    return(
        <div className={style.register} >
            <div>
                <H1 className="register" text={'Crie Sua Conta'} />
                <Form className="register" childrens={inputs} />
            </div>
        </div>
    )

}

export default Register;
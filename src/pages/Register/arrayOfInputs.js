const inputs = [
    { type: 'text', placeholder: 'Nome' },
    { type: 'email', placeholder: 'Email' },
    { type: 'password', placeholder: 'Senha' },
    { type: 'text', placeholder: 'Telefone' },
    { type: 'text', placeholder: 'Estado' },
    { type: 'text', placeholder: 'Cidade' },
    { type: 'text', placeholder: 'N° da casa' },
    { type: 'submit', value: 'Cadastre-se', onClick: (e) => {e.preventDefault()} },
];

export default inputs
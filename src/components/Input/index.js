import React from 'react';
import style from './style.module.scss';

const Input = ({type, value, placeholder, className, onClick}) => {
        
    return (
        <input type={type} className={style[className]} placeholder={ placeholder } value={value} onClick={onClick}/>
     )
}

export default Input;
import React from 'react';
import style from './style.module.scss';

const Form = ({className, childrens, action}) => {

    return (
        <form className={style[className]}>
            {childrens}
        </form>
    )
}

export default Form
import React from 'react';
import style from './style.module.scss'

const H1 = ({className, text}) => {
    return(
        <h1 className={style[className]}>{text}</h1>
    )
}

export default H1;
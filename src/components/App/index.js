import React, { Component } from 'react';
import style from './app.module.scss';
import Routes from './routes';

class App extends Component {

  render() {
    return (
      <div className={style.app}>
        <Routes />
      </div>
    );
  }
}

export default App;

// if (mobile)
// window.visualViewport.height and window.visualViewport.width
import React from 'react';
import {BrowserRouter, Route, Switch, Redirect} from 'react-router-dom';
import Register from '../../pages/Register';

const Routes = () => {
    return(
        <BrowserRouter>
            <Switch>
                <Route exact path="/register" render={ () => <Register className="register" /> }/>
            </Switch>
        </BrowserRouter>
    )

};

export default Routes